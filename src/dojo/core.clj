(ns dojo.core)

;; (defn foo
;;   "I don't do a whole lot."
;;   [x]
;;   (println x "Hello, World!"))

;; (defn greeting
;;   "This is a greeting :P"
;;   [name]
;;   (if (= name "Roger")
;;     "Hello Sir Roger"
;;     (str "Hi " name)))

(def def-map {\I 1 \V 5 \X 10 \L 50})

(defn numros-string-to-int
  [value]
  (reduce + (map-indexed  (fn [i x] 
  (if (get (seq value) (- i 1)) < x)
    (get def-map x)) (seq value)))
    
)

(defn to-int
  [x]
  (let [value (get def-map x)]
    (if value
      value
      (numros-string-to-int value)
      )
    ))
