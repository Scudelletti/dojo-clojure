(ns dojo.core-test
  (:require [clojure.test :refer :all]
            [dojo.core :refer :all]))

;(def n 100)

;(if (< 1 2) "Yeah")
;(if (< 2 1) "Yeah" "Ops")

; (cond
;   (< n 0) "negative"
;   (> n 0) "positive"
;   :else "zero")

; Change key on Map
;(def mymap {:a 1 :b 2})
;(mymap (assoc mymap :a 0)

; Read key from Map
(get {:a 1 :b 2 1 2} :b);


;; (deftest greeting-tests
;;   (testing "Roger is special"
;;     (is (= (greeting "Roger") "Hello Sir Roger")))

;;   (testing "Other names are not special"
;;     (is (= (greeting "Igor") "Hi Igor"))))

;; (deftest let-test
;;   (testing "map and let"
;;     (let [list [1, 2, 3, 4]]
;;       (is (= (map (fn [n] (+ n 1)) [1, 2, 3]) [2, 3, 4]))))

;;   (testing "reduce and let"
;;     (let [list [1, 2, 3, 4]]
;;       (is (= (reduce (fn [n, s] (+ n s)) 0 [1, 2, 3]) 6)))))

(deftest numrus-to-int
  (testing "I is 1"
    (is (= (to-int "I") 1)))
  (testing "V is 5"
    (is (= (to-int "V") 5)))
  (testing "X is 10"
    (is (= (to-int "X") 10)))
  (testing "L is 50"
    (is (= (to-int "L") 50)))
  (testing "II is 2"
    (is (= (to-int "II") 2))))
